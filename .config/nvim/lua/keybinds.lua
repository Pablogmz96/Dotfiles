-- Leader key mapping
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

local function map(m, k, v) -- keybinds mapping function
    vim.keymap.set(m, k, v, { silent = true })
end

-- Keybindings
map('n', '<leader>w', '<CMD>w<CR>') -- Writte the changes on disk
map('n', '<leader>s' , '<CMD>wq<CR>') -- Save the changes and exit vim
map('n', '<leader>q', '<CMD>q!<CR>') -- Exit vim without saving the changes
map('n', '<S-h>', '0')
map('n', '<S-j>', 'G')
map('n', '<S-k>', 'gg')
map('n', '<S-l>', '$')
map('i', '<Tab>', '<C-p>')
map('i', '<A-q>', '<C-t>')
map('v', '<Tab>', '>gv')
map('v', '<S-Tab>', '<gv')
map('n', '<leader>j', '<CMD>HopWord<CR>') -- Activate Hop for words
