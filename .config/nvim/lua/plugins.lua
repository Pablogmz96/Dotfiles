local install_path = vim.fn.stdpath('data') .. '/site/pack/packer/start/packer.nvim'
local install_plugins = false

if vim.fn.empty(vim.fn.glob(install_path)) > 0 then
  print('Installing packer...')
  local packer_url = 'https://github.com/wbthomason/packer.nvim'
  vim.fn.system({'git', 'clone', '--depth', '1', packer_url, install_path})
  print('Done.')

  vim.cmd('packadd packer.nvim')
  install_plugins = true
end

return require('packer').startup(function(use)
	use 'wbthomason/packer.nvim'
	use 'rust-lang/rust.vim'
	use 'jiangmiao/auto-pairs'
	use 'barlog-m/oceanic-primal-vim'
	use 'RRethy/vim-illuminate'
	use "lukas-reineke/indent-blankline.nvim"
	use 'wuelnerdotexe/vim-astro'
	use {
  	'phaazon/hop.nvim',
  	branch = 'v2', -- optional but strongly recommended
  	config = function()
    	-- you can configure Hop the way you like here; see :h hop-config
    	require'hop'.setup {keys = 'etovxqpdygfblzhckisuran'}
  	end
	}
	use {
  	'lewis6991/gitsigns.nvim',
  	config = function()
  		require('gitsigns').setup()
		end
	}

require('illuminate').configure({
	min_count_to_highlight = 2
})
end)
